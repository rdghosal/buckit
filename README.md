# buckit
## A web-based, collaborative activity planner.

### 1. Core Functionality
This app allows registered users to create, read, update, and delete lists of *activities*. Moreover, such operations may be done *simultaneously* by authors of the list.

For each activity, the user has the ability to also save relevant information, such as descriptions, URLs, and free-form memos.

### 2. Technologies
| Framework / Library | Ver. | Purpose |
| ----- | ----- | ----- |
| Flask-RESTX | TBD | Documented REST API |
| MongoDB | TBD | Database |
| React | TBD | User Interface |

### 3. Deployment
This app is to be deployed as Docker containers wherein there is at least a container per technology descripted in Section 2.

### 4. Current Status
| Date | Status |
| ------ | ------|
| 06 Feb 2022 | Started API development |

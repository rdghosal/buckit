import os
from flask import Flask
from flask_restx import Api
from namespaces import user_ns
import pymongo


BASE_PREFIX = "/api"

# --------------------------------------------------------- < Flask-RESTX Api >

# Initialize Api singleton
api = Api(
    title="buckIt!"
    , version="0.1"
    , description="A collaborative activity planning application"
)

# Register namespaces
api.add_namespace(ns=user_ns, path=BASE_PREFIX)


# ------------------------------------------------------------------- < Flask >

client = pymongo.MongoClient(os.getenv("MONGO_URI"))

app = Flask(__name__)
api.init_app(app)

# For production, uncomment the following
# And comment out the last two lines of this file.
# app.run()

# if __name__ == "__main__":
#     app.run(debug=True)
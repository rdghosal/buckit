from dataclasses import dataclass
from typing import Union
from flask_restx import fields, Model


@dataclass
class User:
    name: str
    password: Union[str, None]


# TODO: Change password to hash
user: Model = Model(
    "User",
    {
        "name": fields.String(
            required=True,
            description="Full name of user."
        )
        , "password": fields.String(
            required=True,
            description="Unhashed password of user."
        )
    },
    strict = True
)

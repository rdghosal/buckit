from attr import validate
from flask import jsonify, request
from flask_restx import Namespace, Resource, fields
from models.user import User, user


# Register user namespace
ns = Namespace(
    name='users',
    description='Operations related to users.'
)


ns.model(name="User", model=user)

@ns.marshal_with(user)
@ns.route('/users')
class UserController(Resource):
    """
    Controller for User-related resources.
    """

    @ns.doc("get_user")
    def get(self):
        return jsonify({
            "name": "TestUser",
            "password": "test"
        })

    @ns.doc("post_user")
    @ns.expect(user, validate=True)
    def post(self):
        data = request.get_json()
        print("Received following user data", User(
            name=data["name"],
            password=data["password"]
        ))
        return 201